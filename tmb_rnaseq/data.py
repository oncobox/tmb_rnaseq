import glob
import re

import numpy as np
import pandas as pd
from sklearn import datasets


# iris dataset - for demo and tests

def _load_dataset():
    return datasets.load_iris()


class SourceIris:

    def get_sample_names(self):
        return _load_dataset().target_names.tolist()

    def load_cases(source: str, sample_names):
        res = _load_dataset()

        target_names_mask = np.isin(res.target_names, sample_names)
        indices = np.where(target_names_mask)[0]
        target_mask = np.isin(res.target, indices)

        # keep only requested items
        res.update(dict(
            data=res.data[target_mask],
            target=res.target[target_mask],
            target_names=res.target_names[target_names_mask],
        ))

        # fill out other expected keys
        res.update(dict(
            source='iris',
            sample_name=res.target_names[res.target],
        ))

        return res


# TCGA (moved here from Jupyter notebook)

def _get_samples(glob_patterns, sample_name_regex):
    """Extract sample names from filenames found with the given glob expressions."""
    filenames = []
    for pattern in glob_patterns:
        filenames += [filename.strip() for filename in glob.glob(pattern)]

    sample_names = list(map(
        lambda name: re.search(sample_name_regex, name).group(),
        filenames
    ))

    return filenames, sample_names


def _get_true_set(path):
    vcf = pd.read_csv(path, sep='\t')
    vcf = vcf[vcf["FILTER"] == "PASS"]
    vcf["ID"] = vcf.apply(lambda x: '_'.join([str(x["CHROM"]), str(x["POS"])]), axis=1)
    return set(vcf["ID"])


def _read_spl_rna_features(path, ref):
    sample_name = re.search(r"(?<=spl_)[\w\d_-]*(?=_RNAseq_pon_annotated)", path).group()

    vcf = pd.read_csv(path, sep='\t')
    vcf["FILTER"] = vcf["FILTER"].apply(lambda x: x.split(","))
    vcf["INS"] = vcf.apply(lambda x: 1 if len(x["REF"]) < len(x["ALT"]) else 0, axis=1)
    vcf["DEL"] = vcf.apply(lambda x: 1 if len(x["REF"]) > len(x["ALT"]) else 0, axis=1)
    vcf["len_REF"] = vcf["REF"].apply(lambda x: len(x))
    vcf["len_ALT"] = vcf["ALT"].apply(lambda x: len(x))
    vcf["TOTAL_DP"] = vcf.apply(lambda x: x['sample1.AD'] + x['sample1.AD_1'], axis=1)
    vcf['ExAC_nontcga_ALL'] = vcf['ExAC_nontcga_ALL'].apply(lambda x: 0 if x == "." else x)
    vcf['CA_GT'] = vcf.apply(lambda x: 1 if (x["REF"] == "C" and x["ALT"] == "A") or (x["REF"] == "G" and x["ALT"] == "T") else 0, axis=1)
    vcf['CT_GA'] = vcf.apply(lambda x: 1 if (x["REF"] == "C" and x["ALT"] == "T") or (x["REF"] == "G" and x["ALT"] == "A") else 0, axis=1)
    vcf["MUT_ID"] = vcf.apply(lambda x: x["CHROM"] + "_" + str(x["POS"]), axis=1)
    true_set = _get_true_set(ref)
    vcf["STATUS"] = vcf["MUT_ID"].apply(lambda x: "MUT" if x in true_set else "NOISE")
    vcf = vcf.drop([
        "CHROM", "POS", "ID", "REF", "ALT", "FILTER", "sample1.GT",
        "sample1.MCL","sample1.MCL_1", # empty
        # "ExAC_nontcga_ALL",
        #'sample1.OBAM', 'sample1.OBAMRC', 'sample1.OBF', 'sample1.OBP', 'sample1.OBQ', 'sample1.OBQRC',
        "Func.refGene", #"Gene.refGene", "ExonicFunc.refGene", "AAChange.refGene",
        #"cosmic", "COSM_CNT",
        "MUT_ID"
    ], axis=1)

    vcf = vcf.fillna(0)
    vcf["SAMPLE"] = sample_name
    return vcf


def _get_train_samples_ffpe(sample_names):
    # TODO: rename the function

    train_samples = {}
    for name in sample_names:
        try:
            rna = "/media/volume1/RNA_FFPE/spls/spl_" + name + "_RNAseq_pon_annotated.WXS_4bp.txt"
            dna = "/media/volume1/WXS2_recalc/spls/spl_" + name + "_WXS_annotated.txt"

            train_samples[name] = _read_spl_rna_features(rna, dna)
        except:
            rna = "/media/volume1/RNA_FFPE2_recalc/spls/spl_" + name + "_RNAseq_pon_annotated.WXS_4bp.txt"
            dna = "/media/volume1/WXS3_recalc/spls/spl_" + name + "_WXS_annotated.txt"

            train_samples[name] = _read_spl_rna_features(rna, dna)

    return pd.concat(train_samples.values())


class SourceTcga:
    # TODO: transform hardcoded values into class fields

    def get_sample_names(self):
        sample_name_regex = r"(?<=spl_)[\w\d_-]*(?=_RNAseq_pon_annotated.txt)"

        filenames, sample_names = _get_samples(
            glob_patterns=[
                "/media/volume1/RNA_FFPE/spls/spl*annotated.txt",
                "/media/volume1/RNA_FFPE2_recalc/spls/spl*annotated.txt"
            ],
            sample_name_regex=sample_name_regex
        )
        return sorted(sample_names)

    def load_cases(self, sample_names):
        df = _get_train_samples_ffpe(sample_names)
        X = df.drop(['SAMPLE', 'STATUS'], axis=1)
        return {
            'source': 'TCGA',
            'data': X,
            'feature_names': X.columns,
            'target': (df['STATUS'] != 'NOISE').astype(int),
            'target_names': ['NOISE', 'MUT'],
            'sample_name': df['SAMPLE'],
        }
