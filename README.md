# tmb_rnaseq

TMB calculation based on RNAseq instead of WES.


## Installation
```sh
pip install -r requirements.txt
```

Note on Jupyter notebooks:

- For `diff` it is quite helpful to have a purified version of notebook as a plain `*.py` file

- To view a notebook without Jupyter it is nice to have notebook as `*.html` file

So here is a hook that creates corresponding `*.py` and `*.html` files after each save.

To install it, add to the end of `~/.jupyter/jupyter_notebook_config.py` the following lines:

```python
### If you want to auto-save .html and .py versions of your notebook:
# modified from: https://github.com/ipython/ipython/issues/8009
import os
from subprocess import check_call

def post_save(model, os_path, contents_manager):
    """Post-save hook for converting notebooks to .py scripts."""
    if model['type'] != 'notebook':
        return # only do this for notebooks
    d, fname = os.path.split(os_path)
    check_call(['jupyter', 'nbconvert', '--to', 'script', fname], cwd=d)
    check_call(['jupyter', 'nbconvert', '--to', 'html', fname], cwd=d)

c.FileContentsManager.post_save_hook = post_save
```
