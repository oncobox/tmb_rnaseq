import copy

import pandas as pd


def cleanup_tcga_dataset(dataset):
    if dataset['data'].dtypes['ExAC_nontcga_ALL'] == object:
        res = copy.copy(dataset)
        res['data'] = dataset['data'].copy()
        res['data']['ExAC_nontcga_ALL'] = res['data']['ExAC_nontcga_ALL'].astype(float)

    return dataset
