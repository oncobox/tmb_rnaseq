#!/usr/bin/env python
# coding: utf-8

# In[1]:


__author__ = "Eugenia Zotova"
__maintainer__ = "Alexander Gorelyshev"
__email__ = "alexander.gorelyshev@pm.me"


# In[2]:


get_ipython().run_line_magic('load_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')
get_ipython().run_line_magic('config', "InlineBackend.figure_format = 'retina'")

from IPython.lib.pretty import pprint

import sys
sys.path.append('..')


# In[3]:


import glob
import logging
import os
import re
from datetime import datetime as dt

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
# from xgboost import XGBClassifier

from tmb_rnaseq.data import SourceIris, SourceTcga
from tmb_rnaseq.analysis import cleanup_tcga_dataset


# In[4]:


N_JOBS = 30  # how many jobs we run in parallel (it depends on machine and its load)


# # Load data

# In[5]:


# source = SourceIris()
source = SourceTcga()


# In[6]:


sample_names = source.get_sample_names()
pprint(sample_names, max_seq_length=3)
print("Samples available: {}".format(len(sample_names)))


# In[7]:


get_ipython().run_cell_magic('time', '', "cases = source.load_cases(sample_names)\nprint('Data:', cases['data'].shape)")


# # Prepare datasets for ML

# In[8]:


option = 'standard_train_test'

if option == 'standard_train_test':

    cases_clean = cases
    if cases_clean['source'] == 'TCGA':    
        cases_clean = cleanup_tcga_dataset(cases)

    X = cases_clean['data']
    y = cases_clean['target']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=1)

elif option == 'eugenias_train':
    assert cases['source'] == 'TCGA'
    
    train_sample_names = [
        "TCGA-A6-2684", "TCGA-A6-6650", "TCGA-A6-3810", "TCGA-A6-5659", 
        "TCGA-A6-6780", "TCGA-A6-2672",
        "TCGA-44-6147", "TCGA-44-3917", "TCGA-44-2662", "TCGA-44-2656", 
        "TCGA-44-2668", "TCGA-44-6775",
        "TCGA-A7-A26E", "TCGA-A7-A13E", "TCGA-A7-A0DB", "TCGA-A7-A13D", 
        "TCGA-A7-A26F", "TCGA-A7-A13G",
        "TCGA-BL-A0C8", "TCGA-BL-A13I",
        "TCGA-B2-5635", "TCGA-B2-5633",
        "TCGA-PN-A8MA",
        "TCGA-PL-A8LZ", "TCGA-PL-A8LV", "TCGA-PL-A8LY", "TCGA-PL-A8LX"    
    ]
    train_cases = get_train_samples_ffpe(train_sample_names)
    test_cases = sorted(set(sample_names) - set(train_sample_names))

    clean_tcga_cases(train_cases)
    clean_tcga_cases(test_cases)
    
    X_train, y_train = create_X_y(train_cases)
    X_test, y_test = create_X_y(train_cases)
else:
    raise ValueError('Unknown option.')


# # ML

# In[9]:


get_ipython().run_cell_magic('time', '', '# model = SVC()\nmodel = RandomForestClassifier(n_estimators=100, random_state=1, n_jobs=N_JOBS)\n\n# "Inherited" from the original Eugenia\'s notebook\n# model = XGBClassifier(base_score=0.5, booster=\'gbtree\', colsample_bylevel=1,\n#        colsample_bytree=1.0, gamma=2, learning_rate=0.05, max_delta_step=0,\n#        max_depth=7, min_child_weight=1, missing=None, n_estimators=1500,\n#        n_jobs=1, nthread=24, objective=\'binary:logistic\', random_state=0,\n#        reg_alpha=0.25, reg_lambda=1.5, scale_pos_weight=1, seed=27,\n#        silent=True, subsample=1.0)\n\nmodel.fit(X_train, y_train)')


# In[10]:


# evaluate model on train and test data
_cases = (
    ('train', X_train, y_train),
    ('test', X_test, y_test),
)
for name, X_local, y_local in _cases: 
    y_pred = model.predict(X_local)
    cnf_matrix = confusion_matrix(y_local, y_pred)
    print('\nOn {} data:'.format(name))
    print(pd.crosstab(y_local, y_pred, rownames=['True'], colnames=['Predicted'], margins=True))


# In[11]:


get_ipython().run_cell_magic('time', '', "\nX_local = X_train\ny_local = y_train\n\nif cases['source'] == 'iris': \n    # to play with roc_auc let's pretend we have only two classes\n    y_local = y_local.copy()\n    y_local[y_local != 1] = 0\n\n# evaluate model using CV\nfor scoring in ['accuracy', 'roc_auc']:\n    kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)\n    score = cross_val_score(model, X_local, y_local, cv=kfold, scoring=scoring, n_jobs=N_JOBS)\n    print('{:>10}: {:.2f} {}'.format(scoring, score.mean(), score))\n\nprint()")


# In[ ]:




