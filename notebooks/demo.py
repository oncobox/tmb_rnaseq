#!/usr/bin/env python
# coding: utf-8

# # Demo notebook

# In[1]:


get_ipython().run_line_magic('load_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')
get_ipython().run_line_magic('config', "InlineBackend.figure_format = 'retina'")

from IPython.lib.pretty import pprint

import sys
sys.path.append('..')


# In[2]:


import numpy as np
import pandas as pd
import seaborn as sns

from tmb_rnaseq.data import SourceIris, SourceTcga


# In[3]:


source = SourceIris()


# In[4]:


names = source.get_sample_names()
pprint(names, max_seq_length=3)


# In[5]:


cases = source.load_cases(names[:2])

X = cases['data']
y = cases['target']


# In[6]:


# show some cases
rnd = np.random.RandomState(1)
indices = rnd.choice(X.shape[0], 10, replace=False)
sns.heatmap(X[indices], xticklabels=cases['feature_names'], yticklabels=cases['sample_name'][indices]);


# In[ ]:




